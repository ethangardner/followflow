var system = require('system'),
  page = require('webpage').create(),
  args = system.args;

page.open(args[1].toString(), function(status) {
  page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js", function() {
    
    var links = page.evaluate(function(args) {
      return $('a').map(function(el) {
        var link = this.href;
        if (args[2].trim() == 'external' && link.indexOf(window.location.hostname) === -1) {
          return {
            'url': window.location.href,
            'href': this.href,
            'rel': $(this).attr('rel'),
          }
        }
        else if (args[2].trim() == 'internal' && link.indexOf(window.location.hostname) !== -1) {
          return {
            'url': window.location.href,
            'href': this.href,
            'rel': $(this).attr('rel')
          }
        }
        else if (args[2].trim() == 'all') {
          return {
            'url': window.location.href,
            'href': this.href,
            'rel': $(this).attr('rel'),
          };
        }
      }, args).get();
    }, args);
    
    for (var i = 0; i < links.length; i++) {
      console.log(links[i].url + "\t" + links[i].href + "\t" + links[i].rel);
    }
    phantom.exit();
  });
});
