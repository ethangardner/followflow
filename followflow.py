#!/usr/bin/env python
# encoding: utf-8
import argparse
import os
import subprocess
import time

class FollowFlow():
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-i, --input', action='store', dest='input', required=True, help='Input - can be url or text file containing one URL per line')
        parser.add_argument('-l, --linktype', action='store', dest='linktype', required=True, help='Link type (external, internal, or all)')
        opts = parser.parse_args()

        self.processPage(opts)

    def getUrls(self, opts):
        #open a text file
        lines = [line.strip() for line in open(opts.input)]
        return lines

    def processPage(self, opts):
        basepath = os.path.dirname(os.path.realpath(__file__)) + os.sep
        urls = self.getUrls(opts)
        i = 1

        for url in urls:
            try:
                cmd = subprocess.Popen(['phantomjs', 'findlinks.js', url, opts.linktype], stdout=subprocess.PIPE)
                
                for line in iter(cmd.stdout.readline, b''):
                    print(line.rstrip())
            except Exception:
                print ''

            i = i + 1
            
            time.sleep(1)


if __name__ == '__main__':
    go = FollowFlow()
